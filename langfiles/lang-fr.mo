��    +      t  ;   �      �     �     �     �     �          *     =     O     d     ~     �  m   �       A   -     o     {  *   �     �     �  (   �     �          +     8     D     V     k     w     �  +   �     �     �  %   �       )     "   A     d     t     �  $   �     �  ^   �  P  ,     }
  %   �
     �
  "   �
  #   �
           2     S     l     �     �  x   �     /  3   F     z     �  *   �     �     �  )   �  %        8     X     r     �     �     �      �     �  ,   �     $     9  '   E     m  &   �     �     �     �     �  1   	      ;  w   \                      *          '                 #   (                                                     $          %                 	          +   )   !         "   
   &                                  An error occured App server unavailable Appointment delivery Belgium post Office delivery Belgium store delivery Cityssimo delivery Cityssimo lockers Client access denied ColiPoste agency delivery ColisPoste agency Convenience store delivery Customers with smartphones or ipad will be redirect there. Warning, this url do not allow delivery in belgium Distribution center Enable or disable the check availability  of SoColissimo service. Error code: FO id missing Failed to check signature from posted data Home Home delivery Id user from the SoColissimo back office Invalid encoding field, ignored Invalid transaction number Local trader Mail center Missing signature Post office delivery Post-office Required fields missing Save Secure key from the SoColissimo back office So Colissimo options Supervision This key is required for Socolissimo: Url Supervision Url of the SoColissimo Mobile back office Url of the SoColissimo back office With apointment Wrong FO id Wrong format of the fees Wrong sign or invalid version number Wrong zip code You will be redirect to socolissimo in few moment. If it is not the case, please click button. Project-Id-Version: LibSoColissimo
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-23 08:38+0100
PO-Revision-Date: 2014-07-23 08:45+0100
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ../
X-Poedit-KeywordsList: LibSoColissimo_translate;LibSoColissimo_translate:1,2
X-Generator: Poedit 1.5.4
X-Poedit-SearchPath-0: programs
 Une erreur c'est produite Serveur de l'application indisponible Livraison sur rendez-vous Livraison en bureau de poste belge Livraison chez un commerçant belge Livraison cityssimo Consignes automatiques Cityssimo Accès refusé au client Livraison agence ColiPoste Agence ColiPoste Livraison chez un commerçant Les clients sur smartphones ou ipad serons redirigés ici. Attention, cette url ne permet pas les livraisons en belgique Centre de distribution Activer ou désactiver la vérification du service. Erreur : identifiant FO manquant Échec de la vérification de la signature À domicile Livraison à domicile identifiant FO du back office SoColissimo Code de caractères invalide, ignoré Numéro de transaction invalide Commerçant de proximité Centre Courrier La signature est manquante Livraison en bureau de poste Bureau de poste Des champs requis sont manquants Enregistrer Clé de cryptage du back office So Colissimo Options So Colissimo Supervision Cette clé est requise pour SoColissimo Url de la supervision Url de l'interface mobile So Colissimo Url de l'interface So Colissimo Sur rendez-vous mauvais identifiant FO Mauvais format de frais Mauvaise signature ou numéro de version invalide Le code postal n'est pas correct Vous allez être redirigé vers So Colissimo dans peu de temps. Si ce n'est pas le cas, vous pouvez utiliser ce bouton. 