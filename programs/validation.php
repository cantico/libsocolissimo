<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

require_once dirname(__FILE__).'/utilit/SCFields.php';
require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';



class LibSoColissimo_EventDeliveryConfirmed extends bab_event {
	
	public $so_params;
	
	public function __construct(Array $so_params)
	{
		$this->so_params = $so_params;
	}
	
	
	public function __isset($name)
	{
		return isset($this->so_params[$name]);
	}
	
	
	public function __get($name)
	{
		if (isset($this->so_params[$name])) {
			return $this->so_params[$name];
		}
		
		return null;
	}
	
	
	
	/**
	 * inforamtion supplementaire qui peut �tre stockee dans le panier pour visualisation en back-office
	 * pour le front office, preferer l'utilisation de Func_SoColissimo::getDeliveryMode($code)
	 * @return string
	 */
	public function getDeliveryMode()
	{
		switch($this->so_params['DELIVERYMODE'])
		{
			case 'DOM': return LibSoColissimo_translate('Home delivery');
			
			case 'BPR':
			case 'BRP':	return LibSoColissimo_translate('Post office delivery');
			case 'A2P':
			case 'MRL':
				//TRANSLATORS: Livraison Commerce de proximite
				return LibSoColissimo_translate('Convenience store delivery');
				
			case 'CMT': return LibSoColissimo_translate('Belgium store delivery');

			case 'CIT': return LibSoColissimo_translate('Cityssimo delivery');
	
			case 'ACP': return LibSoColissimo_translate('ColiPoste agency delivery');

			case 'CDI': return LibSoColissimo_translate('Distribution center');
	
			case 'BDP': return LibSoColissimo_translate('Belgium post Office delivery');
	
			case 'RDV': return LibSoColissimo_translate('Appointment delivery');
		}
	
		return null;
	}
}




class LibSoColissimo_validate
{
	private $so;
	
	public function __construct()
	{
		$this->so = new SCFields(bab_rp('DELIVERYMODE'));
	}
	
	
	/**
	 * Get list of posted values or add errors to page
	 * @param Widget_Page $page
	 * @return multitype:string
	 */
	private function getPostedValues($page)
	{
		$return = array();
		
		foreach ($_POST as $key => $val)
		{
			if ($this->so->isAvailableFields($key))
			{
				$return[strtoupper($key)] = stripslashes($val);
			}
		}
		
		
		
		/* GET parameter, the only one */
		$return['TRRETURNURLKO'] = bab_rp('trReturnUrlKo'); /* api 3.0 mobile */
		if (!$return['TRRETURNURLKO'])
		{
			$return['TRRETURNURLKO'] = bab_rp('TRRETURNURLKO'); /* api 4.0 */
		}
		
		foreach ($this->so->getFields(SCFields::REQUIRED) as $field)
		{
			if (!isset($return[$field]))
			{
				$page->addError(LibSoColissimo_translate('This key is required for Socolissimo:').' '.$field);
				
			}
		}
		
		
		
		if (!empty($page->msgerror))
		{
			return false;
		}
		
		
		if ($this->so->isCorrectSignKey($return['SIGNATURE'], $return))
		{
			return $return;
		}
		
		$page->addError(LibSoColissimo_translate('Failed to check signature from posted data'));
		return $return;
	}
	
	
	
	
	
	
	
	/**
	 * 
	 * @return Widget_BabPage
	 */
	public function getPage()
	{
		$W = bab_Widgets();

		
		
		// creer une page pour l'affichage d'erreur eventuelles
		// a verifier si cette page s'affiche dans l'iframe ou dans le site
		$page = $W->BabPage();
		
		$errors_codes = ($tab = bab_rp('ERRORCODE')) ? explode(' ', trim($tab)) : array();
		
		/* If no required error code, start to get the POST data */
		if (!$this->so->checkErrors($errors_codes, SCError::REQUIRED))
		{
			if ($return = $this->getPostedValues($page))
			{
				$event = new LibSoColissimo_EventDeliveryConfirmed($return);
				bab_fireEvent($event);
			}
		}
		else
		{
			if (0 == count($errors_codes))
			{
				$page->addError(LibSoColissimo_translate('An error occured'));
			}
			
			foreach ($errors_codes as $code)
			{
				$page->addError(LibSoColissimo_translate('Error code:').' '.$this->so->getError($code));
			}
		}
		
		return $page;
	}
	
	
}



$validate = new LibSoColissimo_validate();
$validate->getPage()->displayHtml();