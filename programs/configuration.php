<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

class LibSoColissimo_ConfigurationPage
{
	
	
	private function getForm()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		$form = $W->Form(null, $W->VBoxLayout()->setVerticalSpacing(2, 'em'));
		$form->setName('configuration')
			->addClass('BabLoginMenuBackground')
			->addClass('widget-bordered');
		$form->setHiddenValue('tg', bab_rp('tg'));
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Id user from the SoColissimo back office'), $W->LineEdit()->setSize(30), 'id')
		);
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Secure key from the SoColissimo back office'), $W->LineEdit()->setSize(30), 'key')
		);
		/*
		$form->addItem(
			$W->LabelledWidget(
				LibSoColissimo_translate('Average time for preparing your orders'), 
				$W->LineEdit()->setSize(5), 
				'dypreparationtime',
				LibSoColissimo_translate('Average time must match that of Coliposte back office')
			)
		);
		*/
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Seller expedition cost in France'), $W->Checkbox(), 'costseller')
		);
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('expedition in belgium'), $W->Checkbox(), 'exp_bel_active')
		);
		
		/*
		$form->addItem(
			$W->FlowItems(
				$W->LabelledWidget(LibSoColissimo_translate('Overcost for Belgium tax. excl.'), $W->LineEdit()->setSize(5), 'supcostbelg'),
				$W->LabelledWidget(LibSoColissimo_translate('Overcost for Belgium tax. incl.'), $W->LineEdit()->setSize(5), 'costbelgttc')
			)->setSpacing(0,'em', 4,'em')
		);
		
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Additional cost of a delivery with appointment'), $W->LineEdit()->setSize(5), 'overcost',
				LibSoColissimo_translate('Additional cost must match that of Coliposte back office.'))
		);
		*/
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Url of the SoColissimo back office'), $W->LineEdit()->setSize(60), 'url_so')
		);
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Url of the SoColissimo Mobile back office'), $W->LineEdit()->setSize(60), 'url_so_mobile',
					LibSoColissimo_translate('Customers with smartphones or ipad will be redirect there. Warning, this url do not allow delivery in belgium')
			)
		);
		
		/*
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Display'),
			$W->RadioSet()
				->addOption('0', LibSoColissimo_translate('Classic'))
				->addOption('1', LibSoColissimo_translate('Fancybox'))
				->addOption('2',LibSoColissimo_translate('iframe')),
			'display_type')	
		);
		*/
		
		$form->addItem(
			$W->LabelledWidget(LibSoColissimo_translate('Supervision'), $W->Checkbox(), 'sup_active',
					LibSoColissimo_translate('Enable or disable the check availability  of SoColissimo service.'))
		);
		
		$form->addItem(
				$W->LabelledWidget(LibSoColissimo_translate('Url Supervision'), $W->LineEdit()->setSize(60), 'url_sup')
		);
		
		
		// dans le module prestashop
		// la configuration enregistre en plus Allocation socolissimo dans le champ id_socolissimo_allocation
		// et Allocation socolissimo CC dans le champ id_socolissimocc_allocation
		// les deux champs affichent la liste deroulante des "carrier" configures dans prestashop
		
		$form->addItem(
			$W->SubmitButton()
				->setLabel(LibSoColissimo_translate('Save'))
		);
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibSoColissimo/configuration/');
		

		
		$form->setValues(array('configuration' => array(
			'id' 					=> (string) $registry->getValue('id'),
			'key' 					=> (string) $registry->getValue('key'),
	//		'dypreparationtime' 	=> $registry->getValue('dypreparationtime', 0),
			'costseller'			=> $registry->getValue('costseller', true),
			'exp_bel_active'		=> $registry->getValue('exp_bel_active', true),
	//		'supcostbelg'			=> $registry->getValue('supcostbelg'),
	//		'costbelgttc'			=> $registry->getValue('costbelgttc'),
	//		'overcost'				=> $registry->getValue('overcost'),
			'url_so'				=> $registry->getValue('url_so'),
			'url_so_mobile'			=> $registry->getValue('url_so_mobile'),
			'display_type'			=> $registry->getValue('display_type', 2),
			'sup_active'			=> $registry->getValue('sup_active', true),
			'url_sup'				=> $registry->getValue('url_sup')
		)));
		
		return $form;
	}
	


	
	
	public function display()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		$page = $W->BabPage();

		$page->setTitle(LibSoColissimo_translate('So Colissimo options'));
		$page->addItem($this->getForm());
		
		$page->displayHtml();
	}
	
	
	public function save($configuration)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/LibSoColissimo/configuration/');
		
		$registry->setKeyValue('id', $configuration['id']);
		$registry->setKeyValue('key', $configuration['key']);
		// $registry->setKeyValue('dypreparationtime', (int) $configuration['dypreparationtime']);
		$registry->setKeyValue('costseller', $configuration['costseller']);
		$registry->setKeyValue('exp_bel_active', $configuration['exp_bel_active']);
		// $registry->setKeyValue('supcostbelg', $configuration['supcostbelg']);
		// $registry->setKeyValue('costbelgttc', $configuration['costbelgttc']);
		// $registry->setKeyValue('overcost', $configuration['overcost']);
		$registry->setKeyValue('url_so', $configuration['url_so']);
		$registry->setKeyValue('url_so_mobile', $configuration['url_so_mobile']);
		// $registry->setKeyValue('display_type', $configuration['display_type']);
		$registry->setKeyValue('sup_active', $configuration['sup_active']);
		$registry->setKeyValue('url_sup', $configuration['url_sup']);
	}
}


if (!bab_isUserAdministrator())
{
	return;
}

$page = new LibSoColissimo_ConfigurationPage;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();