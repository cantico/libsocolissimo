;<?php/*

[general]
name="LibSoColissimo"
version="0.4"
delete=1
db_prefix="libtimer_"
encoding="UTF-8"
mysql_character_set_database="latin1,utf8"
description="So Colissimo"
author="Paul de Rosanbo (Cantico)"
ov_version="8.0.0"
php_version="5.1.0"
mysql_version="4.1.2"
addon_access_control="0"
icon="so-colissimo.png"
configuration_page="configuration"

[addons]
widgets			=">=0.2.23"
LibTranslate 	= "1.12.0rc3.01"

;*/?>
