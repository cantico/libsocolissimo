<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/SCFields.php';


/**
 * 
 * @property string $ceCivility
 * @property string $ceName
 * @property string $ceFirstName
 * @property string $ceCompanyName
 * @property string $ceAdress1
 * @property string $ceAdress2
 * @property string $ceAdress3
 * @property string $ceAdress4
 * @property string $ceZipCode
 * @property string $ceTown
 * @property string $cePays
 * @property string $ceEntryPhone
 * @property string $ceDeliveryInformation
 * @property string $ceEmail
 * @property string $cePhoneNumber
 * @property string $ceDoorCode1
 * @property string $ceDoorCode2
 * 
 * @property string $dyPreparationTime
 * @property string $dyForwardingCharges
 * @property string $dyForwardingChargesCMT
 * @property string $trClientNumber
 * @property string $trOrderNumber
 * @property string $orderId
 * @property string $dyWeight
 * 
 * 
 */
class LibSoColissimo_Inputs 
{
	/**
	 * The sorted array of url parameters
	 * @var array
	 */
	private $parameters = array();
	
	public function __construct()
	{
		$addon = bab_getAddonInfosInstance('LibSoColissimo');
		
		$charset = bab_charset::getIso();
		if ('ISO-8859-15' === $charset)
		{
			$charset = 'ISO-8859-1';
		}
		
		$this->parameters = array(
				'pudoFOId' 					=> LibSoColissimo_getConfig('id'),

				// informations sur le destinataire
				
				'ceCivility' 				=> null,			// MR | MME | MLE
				'ceName' 					=> null,			// nom
				'ceFirstName' 				=> null,			// prenom
				'ceCompanyName' 			=> null,
				
				'ceAdress1' 				=> null,			// etage, couloir, escalier, n°appartement
				'ceAdress2' 				=> null,			// Entree, batiment, immeuble, residence								
				'ceAdress3' 				=> null,			// Numero et libelle de la voie
				'ceAdress4' 				=> null,			// Lieu-dit ou autre mention speciale
				'ceZipCode' 				=> null,
				'ceTown' 					=> null,
				'cePays' 					=> null, 			// FR | BE  Pays de livraison
				'ceEntryPhone'				=> null,			// Interphone
				'ceDeliveryInformation'		=> null,			// Autres instructions de livraison
				'ceEmail' 					=> null,
				'cePhoneNumber' 			=> null,			// Téléphone portable
																// Jusqu'a 12 caracteres alphanumeriques
																// Doit commencer par 06 ou 07 si le pays est France, et par +324 si le pays est Belgique
				'ceDoorCode1' 				=> null,			// Code porte 1, Jusqu'a 8 caracteres alphanumeriques
				'ceDoorCode2' 				=> null,			// Code porte 2, Jusqu'a 8 caracteres alphanumeriques
				
				
				// Informations sur la commande
		
				'dyPreparationTime' 		=> null, 			// LibSoColissimo_getConfig('dypreparationtime'),
				'dyForwardingCharges' 		=> null,			// Frais d'expedition TTC
				'dyForwardingChargesCMT' 	=> null,			// Frais d'expedition commercant en France TTC
				'trClientNumber' 			=> null,			// num client, 30 caracteres alphanum
				'trOrderNumber'				=> null,			// num commande, 30 caracteres alphanum
				'orderId' 					=> null,			// num transaction, unique pour chaque appel, de 5 a 16 caracteres
																// des 0 sont ajoutes automatiquement si necessaire
				'dyWeight' 					=> null, 			// Jusqu'a 5 caracteres numeriques LibSoColissimo se charge de la conversion en gramme
																// il faut definir la valeur en Kg
				'trFirstOrder'				=> '0',				// 1(Oui) : Les points de retrait hors-domicile ne seront pas proposes		
																// 0(Non) : Les points de retrait hors-domicile seront proposes		
				'trParamPlus' 				=> '',				// 256 caracteres
				
				// International
				
				'trInter' 					=> LibSoColissimo_getConfig('exp_bel_active'),
				'ceLang' 					=> 'FR',			// FR | NL  Langue du destinataire
				
				// Controle de la demande
				
				'numVersion' 				=> '4.0',			// API num version
				'signature'					=> null,			// Set by LibSoColissimo
				'CHARSET' 					=> $charset,
				
				'trReturnUrlKo' 			=> $addon->getUrl().'validation',
				'trReturnUrlOk' 			=> $addon->getUrl().'validation',
				
				
				
				
		);
		
	}
	
	
	/**
	 * 
	 * @param $str
	 * @return mixed
	 */
	public function replaceAccentedChars($str)
	{
		$str = bab_removeDiacritics($str);
		
		$array_unauthorised_api = array(
				';', '€', '~', '#', '{', '(', '[', '|', '\\', '^', ')', ']', '=', '}', '$', '¤', '£', '%', 'μ', '*', '§', '!', '°', '²', '"');
		
		foreach ($array_unauthorised_api as $key => $value)
			$str = str_replace($value, '', $str);
		return $str;
	}
	
	
	public function __set($name, $value)
	{
		switch($name)
		{
			case 'trClientNumber':
			case 'trOrderNumber':
			case 'ceEntryPhone':
				$value = $this->replaceAccentedChars(substr($value, 0, 30));
				break;
				
			case 'orderId':
				$value = $this->formatOrderId($value);
				break;
				
			case 'ceCivility':
				if ('MR' != $value && 'MME' != $value && 'MLE' != $value)
				{
					throw new Exception('Wrong value for ceCivility');
				}
				break;
				
			case 'ceName':
				$value = $this->replaceAccentedChars(substr($value, 0, 34));
				
			case 'ceFirstName':
				$value = $this->replaceAccentedChars(substr($value, 0, 29));
				break;
				 
			case 'ceCompanyName':
			case 'ceAdress1':		
			case 'ceAdress2':
			case 'ceAdress3':
			case 'ceAdress4':
				$value = $this->replaceAccentedChars(substr($value, 0, 38));
				break;
				
			case 'ceZipCode':
			case 'ceEmail':
				$value = $this->replaceAccentedChars($value);
				break;
				
			case 'ceTown':
				$value = $this->replaceAccentedChars(substr($value, 0, 32));
				break;
				
			case 'cePhoneNumber':
				$value = $this->replaceAccentedChars(str_replace(array(' ', '.', '-', ',', ';', '+', '/', '\\', '+', '(', ')'), '', $value));
				break;
				
			case 'dyWeight':
				$value = (string) round($value  * 1000);
				break;
				
			case 'ceDoorCode1':
			case 'ceDoorCode2':
				$value = $this->replaceAccentedChars(substr($value, 0, 8));
				break;
				
			case 'dyForwardingCharges':
			case 'dyForwardingChargesCMT':
				$value = $this->replaceAccentedChars(substr($value, 0, 30));
				break;
				
			case 'ceDeliveryInformation':
				$value = $this->replaceAccentedChars(substr($value, 0, 70));
				break;
				
			case 'cePays':
				if ('FR' != $value && 'BE' != $value)
				{
					throw new Exception('Wrong value for cePays '.$value);
				}
				break;
				
			case 'dyPreparationTime':
				if (strlen($value) > 2 || !is_numeric($value))
				{
					throw new Exception('Wrong configuration for preparation time '.$value);
				}
				break;
				
			default:
				throw new Exception('Missing parameter filter '.$name);
		}
		
		$this->parameters[$name] = $value;
	}
	
	
	
	public function __get($name)
	{
		return $this->parameters[$name];
	}
	
	
	/**
	 * Generate good order id format.
	 *
	 * @param $id
	 * @return string
	 */
	public function formatOrderId($id)
	{
		$str_len = strlen($id);
		while ($str_len < 5)
		{
			$id = '0'.$id;
			$str_len = strlen($id);
		}
		return $id;
	}
	
	
	public function getUrlParameters()
	{
		$so = new SCfields('API');
		$required = $so->getFields(SCFields::REQUIRED);
		
		foreach($this->parameters as $key => $val)
		{
			if (!isset($val) || '' === $val)
			{
				if (in_array($key, $required))
				{
					throw new Exception('Missing parameter value in inputs: '.$key);
				}
				
				$this->parameters[$key] = ''; // set a default value for api request
			}
		}
		
		return $this->parameters;
	}
}