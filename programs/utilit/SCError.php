<?php

/*
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @author Quadra Informatique <modules@quadra-informatique.fr>
 *  @copyright  2007-2014 PrestaShop SA / 1997-2013 Quadra Informatique
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

require_once dirname(__FILE__).'/colissimo.class.php';


class SCError extends LibSoColissimo_Colissimo 
{
	/* Const for better understanding */

	const WARNING = 0;
	const REQUIRED = 1;

	/* Available error list */

	private $errors_list = array();

	public function __construct()
	{

		$this->errors_list = array(
			// Error code returned by the ECHEC URL request (Required)
			SCError::REQUIRED => array(
				'001' => LibSoColissimo_translate('FO id missing'),
				'002' => LibSoColissimo_translate('Wrong FO id'),
				'003' => LibSoColissimo_translate('Client access denied'),
				'004' => LibSoColissimo_translate('Required fields missing'),
				'006' => LibSoColissimo_translate('Missing signature'),
				'007' => LibSoColissimo_translate('Wrong sign or invalid version number'),
				'008' => LibSoColissimo_translate('Wrong zip code'),
				'009' => LibSoColissimo_translate('Wrong format of the Validation back url'),
				'010' => LibSoColissimo_translate('Wrong format of the Failed back url'),
				'011' => LibSoColissimo_translate('Invalid transaction number'),
				'012' => LibSoColissimo_translate('Wrong format of the fees'),
				'015' => LibSoColissimo_translate('App server unavailable'),
				'016' => LibSoColissimo_translate('SGBD unavailable')
			),
			// Error code returned by the Validation URL request (Warning)
			SCError::WARNING => array(
				'501' => LibSoColissimo_translate('Mail field too long, trunked'),
				'502' => LibSoColissimo_translate('Phone field too long, trunked'),
				'503' => LibSoColissimo_translate('Name field too long, trunked'),
				'504' => LibSoColissimo_translate('First name field too long, trunked'),
				'505' => LibSoColissimo_translate('Social reason field too long, trunked'),
				'506' => LibSoColissimo_translate('Floor field too long, trunked'),
				'507' => LibSoColissimo_translate('Hall field too long, trunked'),
				'508' => LibSoColissimo_translate('Locality field too long'),
				'509' => LibSoColissimo_translate('Number and wording access field too long, trunked'),
				'510' => LibSoColissimo_translate('Town field too long, trunked'),
				'511' => LibSoColissimo_translate('Intercom field too long, trunked'),
				'512' => LibSoColissimo_translate('Further Information field too long, trunked'),
				'513' => LibSoColissimo_translate('Door code field too long, trunked'),
				'514' => LibSoColissimo_translate('Door code field too long, trunked'),
				'515' => LibSoColissimo_translate('Customer number too long, trunked'),
				'516' => LibSoColissimo_translate('Transaction order too long, trunked'),
				'517' => LibSoColissimo_translate('ParamPlus field too long, trunked'),
				'131' => LibSoColissimo_translate('Invalid civility, field ignored'),
				'132' => LibSoColissimo_translate('Delay preparation is invalid, ignored'),
				'133' => LibSoColissimo_translate('Invalid weight field, ignored'),
				'134' => LibSoColissimo_translate('Invalid encoding field, ignored')
			)
		);
	}

	/**
	 * Return error type
	 *
	 * @param $number (integer or string)
	 * @param bool $type (SCError::REQUIRED or SCError::WARNING)
	 * @return mixed string|bool
	 */
	public function getError($number, $type = false)
	{
		$number = (string)trim($number);

		if ($type === false || !isset($this->errors_list[$type]))
			$tab = $this->errors_list[SCError::REQUIRED] + $this->errors_list[SCError::WARNING];
		else
			$tab = $this->errors_list[$type];

		return isset($tab[$number]) ? $tab[$number] : false;
	}

	/**
	 * Check the errors list.
	 *
	 * @param $errors
	 * @param bool $type
	 * @return bool
	 */
	public function checkErrors($errors, $type = false)
	{
		foreach ($errors as $num)
			if (($str = $this->getError($num, $type)))
				return $str;

		return false;
	}

}
