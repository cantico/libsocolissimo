<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';

class LibSoColissimo_Colissimo
{
	
	
	private function generateKey($params, $spec)
	{
		$str = '';
		$sortedItems = explode('+', $spec);
		

		foreach ($sortedItems as $key)
		{
			if (isset($params[$key]))
			{
				$str .= $params[$key];
			}
		}

		
		$key = LibSoColissimo_getConfig('key');
		
		if (empty($key))
		{
			throw new Exception('The encrypt key is missing in LibSoColissimo configuration');
		}
	
		return strtolower(sha1($str.$key));
	}
	
	
	
	/**
	 * Generate the signed key for the request
	 *
	 * 
	 * @param $params
	 * @return string
	 */
	public function generateOutputKey($params)
	{
		$spec = 'pudoFOId+ceName+dyPreparationTime+dyForwardingCharges+dyForwardingChargesCMT+trClientNumber+trOrderNumber+orderId+numVersion+ceCivility+ceFirstName+ceCompanyName';
		$spec .= '+ceAdress1+ceAdress2+ceAdress3+ceAdress4+ceZipCode+ceTown+ceEntryPhone+ceDeliveryInformation+ceEmail+cePhoneNumber+ceDoorCode1+ceDoorCode2+dyWeight+trFirstOrder';
		$spec .= '+trParamPlus+trReturnUrlKo+trReturnUrlOk+CHARSET+cePays+trInter+ceLang';
		
		return $this->generateKey($params, $spec);
	}
	
	
	/**
	 * Generate the signed key from the response
	 *
	 *
	 * @param $params
	 * @return string
	 */
	public function generateInputKey($params)
	{
		$str = '';
		foreach ($params as $key => $value)
		{
			$str .= $value;
		}
		
		$key = LibSoColissimo_getConfig('key');

		return strtolower(sha1($str.$key));

	}
	
	
	/**
	 * Generate good order id format.
	 *
	 * @param $id
	 * @return string
	 */
	public function formatOrderId($id)
	{
		$str_len = strlen($id);
		while ($str_len < 5)
		{
			$id = '0'.$id;
			$str_len = strlen($id);
		}
		return $id;
	}
	
	/**
	 * @return bool
	 */
	public function checkAvailibility()
	{
		$url_sup = LibSoColissimo_getConfig('url_sup');
		
		if ($url_sup)
		{
			$ctx = @stream_context_create(array('http' => array('timeout' => 1)));
			$return = @file_get_contents($url_sup, 0, $ctx);
	
			if (ini_get('allow_url_fopen') == 0)
				return true;
			else
			{
				if (!empty($return))
				{
					preg_match('[OK]', $return, $matches);
					if ($matches[0] == 'OK')
						return true;
					return false;
				}
			}
		}
		return true;
	}
}