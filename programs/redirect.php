<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';
require_once dirname(__FILE__).'/utilit/SCFields.php';

class LibSoColissimo_redirect 
{
	private $inputs = array();
	
	public function __construct()
	{
		$this->socolissimo_url = LibSoColissimo_getConfig('url_so');
		$this->description = LibSoColissimo_translate('You will be redirect to socolissimo in few moment. If it is not the case, please click button.');
		
		
		$so = new SCfields('API');
		
		$fields = $so->getFields();
		
		
		foreach ($_GET as $key => $value) {
			if (in_array($key, $fields)) {
				$v = bab_gp($key);
				if ('' !== $v && '0' !== $v)
				{
					$this->inputs[$key] = $v;
				}
			}
		}
		
		
		
		$this->socolissimo_url .= '?trReturnUrlKo='.urlencode($this->inputs['trReturnUrlKo']);
		
		$this->inputs['signature'] = $so->generateOutputKey($this->inputs);
	}
	
	public function getnextinput()
	{
		if (list($name, $value) = each($this->inputs))
		{
			$this->name = bab_toHtml($name);
			$this->value = bab_toHtml($value);
			return true;
		}
		
		return false;
	}
	
	public function output()
	{
		if (empty($this->socolissimo_url))
		{
			die('Missing SoColissimo URL');
		}
		
		die(bab_printTemplate($this, 'addons/LibSoColissimo/redirect.html'));
	}
}


$redirect = new LibSoColissimo_redirect();
$redirect->output();