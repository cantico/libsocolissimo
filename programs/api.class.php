<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2014 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/utilit/inputs.class.php';

class Func_SoColissimo extends bab_functionality
{
	
	
	
	
	public function getDescription()
	{
		return 'So Colissimo';
	}
	
	
	/**
	 * Get the modifiables parameters to send
	 * @return LibSoColissimo_Inputs
	 */
	public function getInputs()
	{
		return new LibSoColissimo_Inputs;
	}
	
	
	/**
	 * Get the URL to use in the iframe
	 * the redirect.php file will create a post to the socolissimo URL with the given parameters
	 * 
	 * @throws Exception		Verify API parameters
	 * 
	 * @param	LibSoColissimo_Parameters		$parameters
	 * 
	 * @return bab_url
	 */
	public function getIframeUrl(LibSoColissimo_Inputs $inputs)
	{
		$url = new bab_url();
		$url->tg = 'addon/LibSoColissimo/redirect';
		
		foreach($inputs->getUrlParameters() as $key => $value)
		{
			$url->$key = $value;
		}
		
		return $url;
	}
	
	
	
	/**
	 * Get internationalized string for delivery mode
	 * version simplifiee du mode de livraison qui peut etre affiche a l'utilisateur sur son panier
	 * 
	 * @param	string	$code	3 letter code
	 * 
	 *  DOM pour domicile
	 *	RDV pour rendez-vous
	 *	BPR pour bureau de Poste fran�ais
	 *	A2P pour commer�ants du r�seau PickUp France
	 *	CIT pour consignes automatiques Cityssimo
	 *	ACP pour Agence Colis
	 *	CDI pour Centre Courrier
	 *	BDP pour Bureau de poste Belge
	 *	CMT pour commer�ants Belges
	 * 
	 * @return string
	 */
	public function getDeliveryMode($code)
	{
		switch($code)
		{
			case 'DOM':	return LibSoColissimo_translate('Home');
			case 'RDV':	return LibSoColissimo_translate('With apointment');
			case 'BPR':	return LibSoColissimo_translate('Post-office');
			case 'A2P':	return LibSoColissimo_translate('Local trader');
			case 'CIT':	return LibSoColissimo_translate('Cityssimo lockers');
			case 'ACP':	return LibSoColissimo_translate('ColisPoste agency');
			case 'CDI':	return LibSoColissimo_translate('Mail center');
			case 'BDP':	return LibSoColissimo_translate('Post-office');
			case 'CMT':	return LibSoColissimo_translate('Local trader');
		}
	}
}